package application;

import views.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MapViewer {

    private static Container container;

    public static void main(String[] args) {

        JFrame frame = new JFrame("GSU 3D MAP");
        frame.setSize(1920, 1080);
        container = frame.getContentPane();

        ////////////////////////////////////////////////////////////// DIRECTION BUTTONS

        // NAME THEM
        JButton map = new JButton("Map");
        JButton admin = new JButton("Administrative Building");
        JButton anderson = new JButton("Anderson Building");
        JButton business = new JButton("Business Building");
        JButton dining = new JButton("Dining Commons");
        JButton deal = new JButton("Deal Building");
        JButton edu = new JButton("Education Building");
        JButton inter = new JButton("Interdisciplinary Building");
        JButton it = new JButton("IT Building");
        JButton lewis = new JButton("Lewis Building");
        JButton lib = new JButton("Henderson Library");
        JButton nursing = new JButton("Nursing Building");
        JButton rac = new JButton("RAC Building");

        // STYLE THEM
        map.setFont(new Font("Arial", Font.PLAIN, 30));
        admin.setFont(new Font("Arial", Font.PLAIN, 30));
        anderson.setFont(new Font("Arial", Font.PLAIN, 30));
        business.setFont(new Font("Arial", Font.PLAIN, 30));
        dining.setFont(new Font("Arial", Font.PLAIN, 30));
        deal.setFont(new Font("Arial", Font.PLAIN, 30));
        edu.setFont(new Font("Arial", Font.PLAIN, 30));
        inter.setFont(new Font("Arial", Font.PLAIN, 30));
        it.setFont(new Font("Arial", Font.PLAIN, 30));
        lewis.setFont(new Font("Arial", Font.PLAIN, 30));
        lib.setFont(new Font("Arial", Font.PLAIN, 30));
        nursing.setFont(new Font("Arial", Font.PLAIN, 30));
        rac.setFont(new Font("Arial", Font.PLAIN, 30));


        // ADD ACTIONS
        map.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                // Remove last view
                container.remove(1);

                // Add new view and validate to show in JPanel
                container.add(new Map());
                container.validate();

            }
        });

        admin.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                // Remove last view
                container.remove(1);

                // Add new view and validate to show in JPanel
                container.add(new Admin());
                container.validate();

            }
        });

        inter.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                // Remove last view
                container.remove(1);

                // Add new view and validate to show in JPanel
                container.add(new Inter());
                container.validate();

            }
        });

        it.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                // Remove last view
                container.remove(1);

                // Add new view and validate to show in JPanel
                container.add(new It());
                container.validate();

            }
        });

        lewis.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                // Remove last view
                container.remove(1);

                // Add new view and validate to show in JPanel
                container.add(new Lewis());
                container.validate();

            }
        });

        lib.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                // Remove last view
                container.remove(1);

                // Add new view and validate to show in JPanel
                container.add(new Lib());
                container.validate();

            }
        });

        nursing.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                // Remove last view
                container.remove(1);

                // Add new view and validate to show in JPanel
                container.add(new Nursing());
                container.validate();

            }
        });

        rac.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                // Remove last view
                container.remove(1);

                // Add new view and validate to show in JPanel
                container.add(new Rac());
                container.validate();

            }
        });

        dining.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                // Remove last view
                container.remove(1);

                // Add new view and validate to show in JPanel
                container.add(new Commons());
                container.validate();

            }
        });

        anderson.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                // Remove last view
                container.remove(1);

                // Add new view and validate to show in JPanel
                container.add(new Anderson());
                container.validate();

            }
        });

        deal.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                // Remove last view
                container.remove(1);

                // Add new view and validate to show in JPanel
                container.add(new Deal());
                container.validate();

            }
        });

        edu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                // Remove last view
                container.remove(1);

                // Add new view and validate to show in JPanel
                container.add(new Education());
                container.validate();

            }
        });

        business.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                // Remove last view
                container.remove(1);

                // Add new view and validate to show in JPanel
                container.add(new Business());
                container.validate();

            }
        });


        ///////////////////////////////////////////////////// ADD ALL CONTROLS OT JPANEL

        JPanel buttons = new JPanel();
        buttons.setLayout(new GridLayout(13, 1, 2, 3));


        buttons.add(map);
        buttons.add(admin);
        buttons.add(anderson);
        buttons.add(business);
        buttons.add(dining);
        buttons.add(deal);
        buttons.add(edu);
        buttons.add(inter);
        buttons.add(it);
        buttons.add(lewis);
        buttons.add(lib);
        buttons.add(nursing);
        buttons.add(rac);
        container.add(buttons, BorderLayout.WEST);

        // INITIALIZE TO MAP VIEW
        container.add(new Map());


        /////////////////////////////////////////////////////////////////// OPEN WINDOW
        // CENTER WINDOW ON SCREEN
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
        int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
        frame.setLocation(x, y);

        // MAKE WINDOW VISIBLE
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

    public static void setAdminView() {
        // Remove last view
        container.remove(1);

        // Add new view and validate to show in JPanel
        container.add(new Admin());
        container.validate();
    }

    public static void setAndersonView() {
        // Remove last view
        container.remove(1);

        // Add new view and validate to show in JPanel
        container.add(new Anderson());
        container.validate();
    }

    public static void setBusinessView() {
        // Remove last view
        container.remove(1);

        // Add new view and validate to show in JPanel
        container.add(new Business());
        container.validate();
    }

    public static void setCommonsView() {
        // Remove last view
        container.remove(1);

        // Add new view and validate to show in JPanel
        container.add(new Commons());
        container.validate();
    }

    public static void setDealView() {
        // Remove last view
        container.remove(1);

        // Add new view and validate to show in JPanel
        container.add(new Deal());
        container.validate();
    }

    public static void setEducationView() {
        // Remove last view
        container.remove(1);

        // Add new view and validate to show in JPanel
        container.add(new Education());
        container.validate();
    }

    public static void setIABView() {
        // Remove last view
        container.remove(1);

        // Add new view and validate to show in JPanel
        container.add(new Inter());
        container.validate();
    }

    public static void setITView() {
        // Remove last view
        container.remove(1);

        // Add new view and validate to show in JPanel
        container.add(new It());
        container.validate();
    }

    public static void setLewisView() {
        // Remove last view
        container.remove(1);

        // Add new view and validate to show in JPanel
        container.add(new Lewis());
        container.validate();
    }

    public static void setLibraryView() {
        // Remove last view
        container.remove(1);

        // Add new view and validate to show in JPanel
        container.add(new Lib());
        container.validate();
    }

    public static void setNursingView() {
        // Remove last view
        container.remove(1);

        // Add new view and validate to show in JPanel
        container.add(new Nursing());
        container.validate();
    }

    public static void setRACView() {
        // Remove last view
        container.remove(1);

        // Add new view and validate to show in JPanel
        container.add(new Rac());
        container.validate();
    }
}

