package objects;

import com.sun.j3d.utils.image.TextureLoader;

import javax.imageio.ImageIO;
import javax.media.j3d.*;
import javax.vecmath.Point3f;
import javax.vecmath.TexCoord2f;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

public class Building extends IndexedQuadArray {

    private float length = 1;
    private float width = 1;
    private float height = 1;
    private String imageName;

    private Appearance ap;

    public Building(float length, float width, float height, String imageName) {
        super(12, QuadArray.COORDINATES | QuadArray.TEXTURE_COORDINATE_2, 24);

        // STORE L, W, H AND BUILDING IMAGE
        this.length = height;
        this.width = width;
        this.height = length;
        this.imageName = imageName;


        // BUILDING LEFT VIEW
        // width, length, height
        Point3f leftBottomLeft = new Point3f(0, 0, 0);
        Point3f leftBottomRight = new Point3f(this.width, 0, 0);
        Point3f leftTopLeft = new Point3f(0, 0, this.height);
        Point3f leftTopRight = new Point3f(this.width, 0, this.height);

        // BUILDING FRONT VIEW
        // width, length, height
        Point3f frontBottomRight = new Point3f(this.width, this.length, 0);
        Point3f frontTopRight = new Point3f(this.width, this.length, this.height);

        // BUILDING RIGHT VIEW
        // width, length, height
        Point3f rightBottomRight = new Point3f(0, this.length, 0);
        Point3f rightTopRight = new Point3f(0, this.length, this.height);

        // COORDINATE PTS
        Point3f[] coords = {leftBottomLeft, leftBottomRight,
                            leftTopLeft, leftTopRight,
                            frontBottomRight, frontTopRight,
                            rightBottomRight, rightTopRight};

        // COORD INDICES
        int[] coordIndices = {  0, 1, 3, 2, // BUILDING LEFT
                                1, 4, 5, 3, // BUILDING FRONT
                                4, 6, 7, 5, // BUILDING RIGHT
                                6, 0, 2, 7, // BUILDING REAR
                                1, 4, 6 ,0, // BUILDING BOTTOM
                                3, 5, 7, 2}; // BUILDING TOP

        // SET THEM
        this.setCoordinates(0, coords);
        this.setCoordinateIndices(0, coordIndices);

        // CREATE TEXTURE INDICES
        TexCoord2f[] tex = {new TexCoord2f(0, 1),new TexCoord2f(1f/3, 1),
                new TexCoord2f(2f/3, 1),new TexCoord2f(1, 1),
                new TexCoord2f(0, 0.5f),new TexCoord2f(1f/3, 0.5f),
                new TexCoord2f(2f/3, 0.5f),new TexCoord2f(1, 0.5f),
                new TexCoord2f(0, 0),new TexCoord2f(1f/3, 0),
                new TexCoord2f(2f/3, 0),new TexCoord2f(1, 0)};

        int[] texIndices ={0,1,5,4, 1,2,6,5, 2,3,7,6, 5,6,10,9, 6,7,11,10, 4,5,9,8};

//        int[] texIndices ={  0, 1, 3, 2, // BUILDING LEFT
//                1, 4, 5, 3, // BUILDING FRONT
//                4, 6, 7, 5, // BUILDING RIGHT
//                6, 0, 2, 7, // BUILDING REAR
//                1, 4, 6 ,0, // BUILDING BOTTOM
//                3, 5, 7, 2}; // BUILDING TOP

        this.setTextureCoordinates(0,0,tex);
        this.setTextureCoordinateIndices(0,texIndices);

        // CREATE APPEARENCE FOR THIS BUILDING
        createAppearance();
    }

    private void createAppearance() {
        Appearance ap = new Appearance();
        BufferedImage buffImage = null;

        String path = "finalproject/images/" + imageName + ".png";

        // Get image
        try {
            buffImage = ImageIO.read(getClass().getClassLoader().getResource(path));
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Failed to get image " + imageName);
        }

        TextureLoader loader = new TextureLoader(buffImage);
        ImageComponent2D image = loader.getImage();

        Texture2D texture = new Texture2D
                (Texture.BASE_LEVEL, Texture.RGBA,
                        image.getWidth(), image.getHeight());
        texture.setImage(0, image);
        texture.setEnable(true);
        texture.setMagFilter(Texture.BASE_LEVEL_LINEAR);
        texture.setMinFilter(Texture.BASE_LEVEL_LINEAR);
        ap.setTexture(texture);
        this.ap = ap;
    }

    public Appearance getAppearance() {
        return this.ap;
    }



}
