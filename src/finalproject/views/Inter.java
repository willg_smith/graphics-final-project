package views;

import com.sun.j3d.utils.behaviors.mouse.MouseRotate;
import com.sun.j3d.utils.behaviors.mouse.MouseTranslate;
import com.sun.j3d.utils.behaviors.mouse.MouseZoom;
import com.sun.j3d.utils.universe.SimpleUniverse;
import objects.Building;

import javax.media.j3d.*;
import javax.swing.*;
import javax.vecmath.Color3f;
import javax.vecmath.Point3f;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector3f;
import java.awt.*;

public class Inter extends JPanel {

    public static final float length = 0.96f;
    public static final float width = 0.72f;
    public static final float depth = 0.025f;


    public Inter() {

        System.setProperty("sun.awt.noerasebackground", "true");

        //Set the layout
        setLayout(new BorderLayout());

        // Get the preferred graphics configuration for the default screen
        GraphicsConfiguration config = SimpleUniverse.getPreferredConfiguration();

        // Create a Canvas3D using the preferred configuration
        Canvas3D canvas = new Canvas3D(config);

        // Create a Simple Universe with view branch
        SimpleUniverse universe = new SimpleUniverse(canvas);

        // Create the root of the branch graph
        BranchGroup group = createSceneGraph();

        // Add the canvas into the center of the screen
        add("Center", canvas);

        // This will move the ViewPlatform back so the objects in the scene can be viewed
        universe.getViewingPlatform().setNominalViewingTransform();

        // Set the render distance of objects
        universe.getViewer().getView().setBackClipDistance(100.0);

        // Add the branch into the Universe
        universe.addBranchGraph(group);

    }

    private BranchGroup createSceneGraph() {
        BranchGroup root = new BranchGroup();

        // Scaling transform
        Transform3D tr = new Transform3D();
        tr.rotX(Math.PI/3);
        tr.setTranslation(new Vector3d(-0.35, 0.45, 0.0));

        // Master Transform Group
        TransformGroup parentTg = new TransformGroup(tr);
        parentTg.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        parentTg.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
        root.addChild(parentTg);

        // Appearance
        Appearance ap = new Appearance();

        // Materials
        Material mat = new Material();
        mat.setLightingEnable(true);
        mat.setShininess(30);
        ap.setMaterial(mat);

        ////////////////////////////////////////////////////////////////////////

        // Building Transform Group
        Transform3D buildingTr = new Transform3D();
        buildingTr.setScale(0.8f);
        buildingTr.setTranslation(new Vector3f(0.38f, 0.0f, .455f));
        TransformGroup businessTg = new TransformGroup(buildingTr);

        // Create building
        Building buildingGeometry = new Building(.5f, .2f, 0.1f, "Interdisciplinary");
        Geometry geometry = buildingGeometry;
        Appearance buildingAppearance = buildingGeometry.getAppearance();
        PolygonAttributes paBusiness = new PolygonAttributes(PolygonAttributes.POLYGON_FILL,
                PolygonAttributes.CULL_NONE,0);
        paBusiness.setBackFaceNormalFlip(true);
        buildingAppearance.setPolygonAttributes(paBusiness);
        Shape3D shapeBusiness = new Shape3D(geometry, buildingAppearance);
        businessTg.addChild(shapeBusiness);
        parentTg.addChild(businessTg);


        /////////////////////////////////////////////////////////////////////////////////////////////////////

        BoundingSphere bounds = new BoundingSphere();

        // Mouse stuff
        // Handle mouse rotate
        MouseRotate rotate = new MouseRotate(parentTg);
        rotate.setFactor(.01f);
        rotate.setSchedulingBounds(bounds);
        parentTg.addChild(rotate);

        // Handle mouse translate
        MouseTranslate translate = new MouseTranslate(parentTg);
        translate.setFactor(.01f);
        translate.setSchedulingBounds(bounds);
        parentTg.addChild(translate);

        // Handle zoom
        MouseZoom zoom = new MouseZoom(parentTg);
        zoom.setFactor(.01f);
        zoom.setSchedulingBounds(bounds);
        parentTg.addChild(zoom);

        parentTg.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        parentTg.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);

        // Background
        Background background = new Background(1.0f, 1.0f, 1.0f);
        background.setColor(new Color3f(Color.gray));
        background.setApplicationBounds(bounds);
        root.addChild(background);

        // Lighting
        AmbientLight light = new AmbientLight(true, new Color3f(Color.BLACK));
        light.setInfluencingBounds(bounds);
        root.addChild(light);


        PointLight ptlight = new PointLight(new Color3f(Color.white), new Point3f(0.5f, 0.5f, 1f), new Point3f(1f, 0.2f, 0f));
        ptlight.setInfluencingBounds(bounds);
        root.addChild(ptlight);

        return root;
    }
}
