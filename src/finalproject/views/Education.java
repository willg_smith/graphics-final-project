package views;

import com.sun.j3d.utils.behaviors.mouse.MouseRotate;
import com.sun.j3d.utils.behaviors.mouse.MouseTranslate;
import com.sun.j3d.utils.behaviors.mouse.MouseZoom;
import com.sun.j3d.utils.geometry.GeometryInfo;
import com.sun.j3d.utils.image.TextureLoader;
import com.sun.j3d.utils.universe.SimpleUniverse;
import objects.Building;

import javax.media.j3d.*;
import javax.swing.*;
import javax.vecmath.*;
import java.awt.*;
import java.net.URL;

public class Education extends JPanel {

    public static final float length = 0.96f;
    public static final float width = 0.72f;
    public static final float depth = 0.025f;

    public Education() {

        System.setProperty("sun.awt.noerasebackground", "true");

        //Set the layout
        setLayout(new BorderLayout());

        // Get the preferred graphics configuration for the default screen
        GraphicsConfiguration config = SimpleUniverse.getPreferredConfiguration();

        // Create a Canvas3D using the preferred configuration
        Canvas3D canvas = new Canvas3D(config);

        // Create a Simple Universe with view branch
        SimpleUniverse universe = new SimpleUniverse(canvas);

        // Create the root of the branch graph
        BranchGroup group = createSceneGraph();

        // Add the canvas into the center of the screen
        add("Center", canvas);

        // This will move the ViewPlatform back so the objects in the scene can be viewed
        universe.getViewingPlatform().setNominalViewingTransform();

        // Set the render distance of objects
        universe.getViewer().getView().setBackClipDistance(100.0);

        // Add the branch into the Universe
        universe.addBranchGraph(group);

    }

    private BranchGroup createSceneGraph() {
        BranchGroup root = new BranchGroup();

        // Scaling transform
        Transform3D tr = new Transform3D();
        tr.rotX(Math.PI/3);
        tr.setTranslation(new Vector3d(-0.35, 0.45, 0.0));

        // Master Transform Group
        TransformGroup parentTg = new TransformGroup(tr);
        parentTg.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        parentTg.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
        root.addChild(parentTg);

        // Appearance
        Appearance ap = new Appearance();

        // Materials
        Material mat = new Material();
        mat.setLightingEnable(true);
        mat.setShininess(30);
        ap.setMaterial(mat);

        ////////////////////////////////////////////////////////////////////////
        Transform3D eduTR = new Transform3D();
        eduTR.rotY(30.9);
        eduTR.setScale(0.8f);
        eduTR.setTranslation(new Vector3f(0.285f, 0.0f, 0.47f));
        TransformGroup eduTG = new TransformGroup(eduTR);

        // Create education building
        Building eduGeo = new Building(.5f, 0.29f, 0.1f, "Education");
        Geometry geometry = eduGeo;
        Appearance buildingAppearance = eduGeo.getAppearance();
        PolygonAttributes paBusiness = new PolygonAttributes(PolygonAttributes.POLYGON_FILL,
                PolygonAttributes.CULL_NONE,0);
        paBusiness.setBackFaceNormalFlip(true);
        buildingAppearance.setPolygonAttributes(paBusiness);
        Shape3D shapeBusiness = new Shape3D(geometry, buildingAppearance);
        eduTG.addChild(shapeBusiness);
        parentTg.addChild(eduTG);

        // spin.addChild(andersonTG);


        /////////////////////////////////////////////////////////////////////////////////////////////////////

        BoundingSphere bounds = new BoundingSphere();

        // Mouse stuff
        // Handle mouse rotate
        MouseRotate rotate = new MouseRotate(parentTg);
        rotate.setFactor(.01f);
        rotate.setSchedulingBounds(bounds);
        parentTg.addChild(rotate);

        // Handle mouse translate
        MouseTranslate translate = new MouseTranslate(parentTg);
        translate.setFactor(.01f);
        translate.setSchedulingBounds(bounds);
        parentTg.addChild(translate);

        // Handle zoom
        MouseZoom zoom = new MouseZoom(parentTg);
        zoom.setFactor(.01f);
        zoom.setSchedulingBounds(bounds);
        parentTg.addChild(zoom);

        parentTg.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        parentTg.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);

        // Background
        Background background = new Background(1.0f, 1.0f, 1.0f);
        background.setColor(new Color3f(Color.gray));
        background.setApplicationBounds(bounds);
        root.addChild(background);

        // Lighting
        AmbientLight light = new AmbientLight(true, new Color3f(Color.BLACK));
        light.setInfluencingBounds(bounds);
        root.addChild(light);


        PointLight ptlight = new PointLight(new Color3f(Color.white), new Point3f(0.5f, 0.5f, 1f), new Point3f(1f, 0.2f, 0f));
        ptlight.setInfluencingBounds(bounds);
        root.addChild(ptlight);

        return root;
    }
}