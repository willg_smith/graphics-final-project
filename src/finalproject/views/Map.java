package views;

import application.MapViewer;
import com.sun.j3d.utils.behaviors.mouse.MouseRotate;
import com.sun.j3d.utils.behaviors.mouse.MouseTranslate;
import com.sun.j3d.utils.behaviors.mouse.MouseZoom;
import com.sun.j3d.utils.geometry.GeometryInfo;
import com.sun.j3d.utils.image.TextureLoader;
import com.sun.j3d.utils.picking.PickCanvas;
import com.sun.j3d.utils.picking.PickResult;
import com.sun.j3d.utils.universe.SimpleUniverse;
import objects.Building;

import javax.media.j3d.*;
import javax.swing.*;
import javax.vecmath.*;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.URL;

public class Map extends JPanel implements MouseListener {

    public static final float length = 0.96f;
    public static final float width = 0.72f;
    public static final float depth = 0.025f;
    static Canvas3D canvas;
    static PickCanvas pickCanvas;
    static boolean clickBuildings = true;

    public Map() {

        System.setProperty("sun.awt.noerasebackground", "true");

        MapViewer mv = new MapViewer();

        //Set the layout, this is just a standard thing.
        setLayout(new BorderLayout());

        //Get the preferred graphics configuration for the default screen
        GraphicsConfiguration config = SimpleUniverse.getPreferredConfiguration();

        //Create a Canvas3D using the preferred configuration
        canvas = new Canvas3D(config);

        //Create a Simple Universe with view branch
        SimpleUniverse universe = new SimpleUniverse(canvas);

        //Create the root of the branch graph
        BranchGroup group = createSceneGraph();

        //Add the canvas into the center of the screen
        add("Center", canvas);

        //This will move the ViewPlatform back so the objects in the scene can be viewed
        universe.getViewingPlatform().setNominalViewingTransform();

        //Set the render distance of objects
        universe.getViewer().getView().setBackClipDistance(100.0);

        //Add the branch into the Universe
        universe.addBranchGraph(group);

        pickCanvas = new PickCanvas(canvas, group);

        pickCanvas.setMode(PickCanvas.BOUNDS);

        canvas.addMouseListener(this);

    }

    private BranchGroup createSceneGraph() {
        BranchGroup root = new BranchGroup();
        Shape3D shape = new Shape3D();
        shape.setGeometry(createGSUMap().getIndexedGeometryArray());

        // Scaling transform
        Transform3D tr = new Transform3D();
        tr.rotX(Math.PI / 3);
        tr.setTranslation(new Vector3d(-0.35, 0.45, 0.0));

        // Master Transform Group
        TransformGroup spin = new TransformGroup(tr);
        spin.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        spin.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
        root.addChild(spin);

        // Appearance
        Appearance ap = createTextureAppearance();

        // Materials
        Material mat = new Material();
        mat.setLightingEnable(true);
        mat.setShininess(30);
        ap.setMaterial(mat);

        // Map Transform Group
        TransformGroup tg = new TransformGroup();
        tg.addChild(shape);
        spin.addChild(tg);
        shape.setAppearance(ap);

        // Business Building Transform Group
        Transform3D businessTR = new Transform3D();
        businessTR.rotY(30.5);
        businessTR.setScale(0.10f);
        businessTR.setTranslation(new Vector3f(0.38f, 0.0f, .455f));
        TransformGroup businessTG = new TransformGroup(businessTR);

        // Create business building
        Building businessGeo = new Building(.5f, .2f, 0.1f, "Business");
        Geometry business = businessGeo;
        Appearance apBusiness = businessGeo.getAppearance();
        PolygonAttributes paBusiness = new PolygonAttributes(PolygonAttributes.POLYGON_FILL,
                PolygonAttributes.CULL_NONE, 0);
        paBusiness.setBackFaceNormalFlip(true);
        apBusiness.setPolygonAttributes(paBusiness);
        Shape3D shapeBusiness = new Shape3D(business, apBusiness);
        shapeBusiness.setUserData("bus");
        businessTG.addChild(shapeBusiness);
        spin.addChild(businessTG);

        // IT Building Transform Group
        Transform3D itTR = new Transform3D();
        itTR.rotY(30.5);
        itTR.setScale(0.10f);
        itTR.setTranslation(new Vector3f(0.361f, 0.0f, 0.415f));
        TransformGroup itTG = new TransformGroup(itTR);

        // Create IT building
        Building ITGeo = new Building(.5f, 0.2f, 0.1f, "IT");
        Geometry IT = ITGeo;
        Appearance apIT = ITGeo.getAppearance();
        PolygonAttributes paIT = new PolygonAttributes(PolygonAttributes.POLYGON_FILL,
                PolygonAttributes.CULL_NONE, 0);
        paIT.setBackFaceNormalFlip(true);
        apIT.setPolygonAttributes(paIT);
        Shape3D shapeIT = new Shape3D(IT, apIT);
        shapeIT.setUserData("it");
        itTG.addChild(shapeIT);
        spin.addChild(itTG);

        // Edu Building Transform Group
        Transform3D eduTR = new Transform3D();
        eduTR.rotY(30.9);
        eduTR.setScale(0.10f);
        eduTR.setTranslation(new Vector3f(0.285f, 0.0f, 0.47f));
        TransformGroup eduTG = new TransformGroup(eduTR);

        // Create education building
        Building eduGeo = new Building(.5f, 0.29f, 0.1f, "Education");
        Geometry edu = eduGeo;
        Appearance apEdu = eduGeo.getAppearance();
        PolygonAttributes paEdu = new PolygonAttributes(PolygonAttributes.POLYGON_FILL,
                PolygonAttributes.CULL_NONE, 0);
        paEdu.setBackFaceNormalFlip(true);
        apEdu.setPolygonAttributes(paEdu);
        Shape3D shapeEdu = new Shape3D(edu, apEdu);
        shapeEdu.setUserData("edu");
        eduTG.addChild(shapeEdu);
        spin.addChild(eduTG);

        // Nursing Building Transform Group
        Transform3D nursingTR = new Transform3D();
        nursingTR.rotY(30.9);
        nursingTR.setScale(0.10f);
        nursingTR.setTranslation(new Vector3f(0.315f, 0.0f, 0.5f));
        TransformGroup nursingTG = new TransformGroup(nursingTR);

        // Create nursing building
        Building nursingGeo = new Building(.5f, 0.3f, 0.1f, "Nursing");
        Geometry nursing = nursingGeo;
        Appearance apNursing = nursingGeo.getAppearance();
        PolygonAttributes paNursing = new PolygonAttributes(PolygonAttributes.POLYGON_FILL,
                PolygonAttributes.CULL_NONE, 0);
        paNursing.setBackFaceNormalFlip(true);
        apNursing.setPolygonAttributes(paNursing);
        Shape3D shapeNursing = new Shape3D(nursing, apNursing);
        nursingTG.addChild(shapeNursing);
        shapeNursing.setUserData("nur");
        spin.addChild(nursingTG);

        // RAC Transform Group
        Transform3D racTR = new Transform3D();
        racTR.rotY(.16f);
        racTR.setScale(0.10f);
        racTR.setTranslation(new Vector3f(0.11f, 0.0f, 0.74f));
        TransformGroup racTG = new TransformGroup(racTR);

        // Create RAC building
        Building racGeo = new Building(.72f, 0.52f, 0.1f, "RAC");
        Geometry rac = racGeo;
        Appearance apRac = racGeo.getAppearance();
        PolygonAttributes paRac = new PolygonAttributes(PolygonAttributes.POLYGON_FILL,
                PolygonAttributes.CULL_NONE, 0);
        paRac.setBackFaceNormalFlip(true);
        apRac.setPolygonAttributes(paRac);
        Shape3D shapeRac = new Shape3D(rac, apRac);
        shapeRac.setUserData("rac");
        racTG.addChild(shapeRac);
        spin.addChild(racTG);

        // Admin Building Transform Group
        Transform3D adminTR = new Transform3D();
        adminTR.rotY(-1f);
        adminTR.setScale(0.10f);
        adminTR.setTranslation(new Vector3f(0.495f, 0.0f, 0.265f));
        TransformGroup adminTG = new TransformGroup(adminTR);

        // Create Admin building
        Building adminGeo = new Building(.25f, 0.12f, 0.1f, "Administrative");
        Geometry admin = adminGeo;
        Appearance apAdmin = adminGeo.getAppearance();
        PolygonAttributes paAdmin = new PolygonAttributes(PolygonAttributes.POLYGON_FILL,
                PolygonAttributes.CULL_NONE, 0);
        paAdmin.setBackFaceNormalFlip(true);
        apAdmin.setPolygonAttributes(paAdmin);
        Shape3D shapeAdmin = new Shape3D(admin, apAdmin);
        adminTG.addChild(shapeAdmin);
        shapeAdmin.setUserData("adm");
        spin.addChild(adminTG);

        // Anderson Building Transform Group
        Transform3D andersonTR = new Transform3D();
        andersonTR.rotY(2.9f);
        andersonTR.setScale(0.10f);
        andersonTR.setTranslation(new Vector3f(0.51f, 0.0f, 0.275f));
        TransformGroup andersonTG = new TransformGroup(andersonTR);

        // Create Anderson building
        Building andersonGeo = new Building(.25f, 0.12f, 0.1f, "Anderson");
        Geometry anderson = andersonGeo;
        Appearance apAnderson = andersonGeo.getAppearance();
        PolygonAttributes paAnderson = new PolygonAttributes(PolygonAttributes.POLYGON_FILL,
                PolygonAttributes.CULL_NONE, 0);
        paAnderson.setBackFaceNormalFlip(true);
        apAnderson.setPolygonAttributes(paAnderson);
        Shape3D shapeAnderson = new Shape3D(anderson, apAnderson);
        shapeAnderson.setUserData("and");
        andersonTG.addChild(shapeAnderson);
        spin.addChild(andersonTG);

        // Deal Building Transform Group
        Transform3D dealTR = new Transform3D();
        dealTR.rotY(1.3f);
        dealTR.setScale(0.10f);
        dealTR.setTranslation(new Vector3f(0.45f, 0.0f, 0.29f));
        TransformGroup dealTG = new TransformGroup(dealTR);

        // Create Deal building
        Building dealGeo = new Building(.25f, 0.12f, 0.1f, "Deal");
        Geometry deal = dealGeo;
        Appearance apDeal = dealGeo.getAppearance();
        PolygonAttributes paDeal = new PolygonAttributes(PolygonAttributes.POLYGON_FILL,
                PolygonAttributes.CULL_NONE, 0);
        paDeal.setBackFaceNormalFlip(true);
        apDeal.setPolygonAttributes(paDeal);
        Shape3D shapeDeal = new Shape3D(deal, apDeal);
        shapeDeal.setUserData("dea");
        dealTG.addChild(shapeDeal);
        spin.addChild(dealTG);

        // Commons Building Transform Group
        Transform3D commonsTR = new Transform3D();
        commonsTR.rotY(-1f);
        commonsTR.setScale(0.10f);
        commonsTR.setTranslation(new Vector3f(0.63f, 0.0f, 0.39f));
        TransformGroup commonsTG = new TransformGroup(commonsTR);

        // Create Commons building
        Building commonsGeo = new Building(.45f, 0.45f, 0.1f, "Commons");
        Geometry commons = commonsGeo;
        Appearance apCommons = commonsGeo.getAppearance();
        PolygonAttributes paCommons = new PolygonAttributes(PolygonAttributes.POLYGON_FILL,
                PolygonAttributes.CULL_NONE, 0);
        paCommons.setBackFaceNormalFlip(true);
        apCommons.setPolygonAttributes(paCommons);
        Shape3D shapeCommons = new Shape3D(commons, apCommons);
        shapeCommons.setUserData("com");
        commonsTG.addChild(shapeCommons);
        spin.addChild(commonsTG);

        // IAB Transform Group
        Transform3D iabTR = new Transform3D();
        iabTR.rotY(30.5f);
        iabTR.setScale(0.10f);
        iabTR.setTranslation(new Vector3f(0.39f, 0.0f, 0.4f));
        TransformGroup iabTG = new TransformGroup(iabTR);

        // Create IAB
        Building iabGeo = new Building(.3f, 0.2f, 0.1f, "Interdisciplinary");
        Geometry iab = iabGeo;
        Appearance apIAB = iabGeo.getAppearance();
        PolygonAttributes paIAB = new PolygonAttributes(PolygonAttributes.POLYGON_FILL,
                PolygonAttributes.CULL_NONE, 0);
        paIAB.setBackFaceNormalFlip(true);
        apIAB.setPolygonAttributes(paIAB);
        Shape3D shapeIAB = new Shape3D(iab, apIAB);
        shapeIAB.setUserData("iab");
        iabTG.addChild(shapeIAB);
        spin.addChild(iabTG);

        // Lewis Transform Group
        Transform3D lewisTR = new Transform3D();
        lewisTR.rotY(0.5f);
        lewisTR.setScale(0.10f);
        lewisTR.setTranslation(new Vector3f(0.42f, 0.0f, 0.27f));
        TransformGroup lewisTG = new TransformGroup(lewisTR);

        // Create Lewis Building
        Building lewisGeo = new Building(.25f, 0.12f, 0.1f, "Lewis");
        Geometry lewis = lewisGeo;
        Appearance apLewis = lewisGeo.getAppearance();
        PolygonAttributes paLewis = new PolygonAttributes(PolygonAttributes.POLYGON_FILL,
                PolygonAttributes.CULL_NONE, 0);
        paLewis.setBackFaceNormalFlip(true);
        apLewis.setPolygonAttributes(paLewis);
        Shape3D shapeLewis = new Shape3D(lewis, apLewis);
        shapeLewis.setUserData("lew");
        lewisTG.addChild(shapeLewis);
        spin.addChild(lewisTG);

        // Library Transform Group
        Transform3D libraryTR = new Transform3D();
        libraryTR.rotY(-1f);
        libraryTR.setScale(0.10f);
        libraryTR.setTranslation(new Vector3f(0.47f, 0.0f, 0.32f));
        TransformGroup libraryTG = new TransformGroup(libraryTR);

        // Create Library Building
        Building libraryGeo = new Building(.5f, 0.4f, 0.1f, "Library");
        Geometry library = libraryGeo;
        Appearance apLibrary = libraryGeo.getAppearance();
        PolygonAttributes paLibrary = new PolygonAttributes(PolygonAttributes.POLYGON_FILL,
                PolygonAttributes.CULL_NONE, 0);
        paLibrary.setBackFaceNormalFlip(true);
        apLibrary.setPolygonAttributes(paLibrary);
        Shape3D shapeLibrary = new Shape3D(library, apLibrary);
        shapeLibrary.setUserData("lib");
        libraryTG.addChild(shapeLibrary);
        spin.addChild(libraryTG);


        //////////////////////////////////////

        BoundingSphere bounds = new BoundingSphere();

        // Mouse stuff
        // Handle mouse rotate
        MouseRotate rotate = new MouseRotate(spin);
        rotate.setFactor(.01f);
        rotate.setSchedulingBounds(bounds);
        spin.addChild(rotate);

        // Handle mouse translate
        MouseTranslate translate = new MouseTranslate(spin);
        translate.setFactor(.01f);
        translate.setSchedulingBounds(bounds);
        spin.addChild(translate);

        // Handle zoom
        MouseZoom zoom = new MouseZoom(spin);
        zoom.setFactor(.01f);
        zoom.setSchedulingBounds(bounds);
        spin.addChild(zoom);

        tg.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        tg.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);

        // Background
        Background background = new Background(1.0f, 1.0f, 1.0f);
        background.setColor(new Color3f(Color.gray));
        background.setApplicationBounds(bounds);
        root.addChild(background);

        // Lighting
        AmbientLight light = new AmbientLight(true, new Color3f(Color.BLACK));
        light.setInfluencingBounds(bounds);
        root.addChild(light);


        PointLight ptlight = new PointLight(new Color3f(Color.white), new Point3f(0.5f, 0.5f, 1f), new Point3f(1f, 0.2f, 0f));
        ptlight.setInfluencingBounds(bounds);
        root.addChild(ptlight);

        return root;
    }

    private GeometryInfo createGSUMap() {

        IndexedQuadArray map = new IndexedQuadArray(8, IndexedQuadArray.COORDINATES | IndexedQuadArray.NORMALS | IndexedQuadArray.TEXTURE_COORDINATE_2, 24);
        Point3f[] vertices = {
                new Point3f(0, 0, 0), new Point3f(0, 0, length), new Point3f(width, 0, length), new Point3f(width, 0, 0),                //Top face
                new Point3f(0, -depth, 0), new Point3f(0, -depth, length), new Point3f(width, -depth, length), new Point3f(width, -depth, 0)        //Bottom face
        };
        int[] indices = {0, 1, 2, 3, 0, 4, 5, 1, 1, 5, 6, 2, 2, 6, 7, 3, 3, 7, 4, 0, 4, 5, 6, 7};    //Top,  Left,  Back,  Right,  Front,  Bottom
        TexCoord2f[] textCoord = {new TexCoord2f(1.0f, 1.0f),
                new TexCoord2f(0.0f, 1.0f), new TexCoord2f(0.0f, 0.0f),
                new TexCoord2f(1.0f, 0.0f)};
        int[] texIndices = {3, 0, 1, 2};
        Vector3f[] normals = {new Vector3f(0.0f, 1.0f, 0.0f),
                new Vector3f(-1.0f, 0.0f, 0.0f),
                new Vector3f(0.0f, 0.0f, -1.0f),
                new Vector3f(1.0f, 0.0f, 0.0f),
                new Vector3f(0.0f, 0.0f, 1.0f),
                new Vector3f(0.0f, -1.0f, 0.0f)};
        int[] normalIndices = {0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3,
                4, 4, 4, 4, 5, 5, 5, 5};
        map.setCoordinates(0, vertices);
        map.setCoordinateIndices(0, indices);
        map.setTextureCoordinates(0, 0, textCoord);
        map.setTextureCoordinateIndices(0, 0, texIndices);
        map.setNormals(0, normals);
        map.setNormalIndices(0, normalIndices);
        return new GeometryInfo(map);

    }

    Appearance createTextureAppearance() {
        Appearance ap = new Appearance();
        URL filename = getClass().getClassLoader().getResource("finalproject/images/Georgia_Southern_Map.png");
        TextureLoader loader = new TextureLoader(filename, this);
        ImageComponent2D image = loader.getImage();

        Texture2D texture = new Texture2D
                (Texture.BASE_LEVEL, Texture.RGBA,
                        image.getWidth(), image.getHeight());
        texture.setImage(0, image);
        texture.setEnable(true);
        texture.setMagFilter(Texture.BASE_LEVEL_LINEAR);
        texture.setMinFilter(Texture.BASE_LEVEL_LINEAR);
        ap.setTexture(texture);
        return ap;
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

        Map.pickCanvas.setShapeLocation(e);

        PickResult result = pickCanvas.pickClosest();


        Shape3D s = null;
        try {
            s = (Shape3D) result.getNode(PickResult.SHAPE3D);
        } catch (Exception ex) {

        }

        if (Map.clickBuildings == false) return;

        if (s == null) return;

        if (s.getUserData() != null) {
            String description = s.getUserData().toString().toLowerCase();

            switch (description) {
                case "lib" : MapViewer.setLibraryView(); break;
                case "com" : MapViewer.setCommonsView(); break;
                case "and" : MapViewer.setAndersonView(); break;
                case "dea" : MapViewer.setDealView(); break;
                case "iab" : MapViewer.setIABView(); break;
                case "rac" : MapViewer.setRACView(); break;
                case "lew" : MapViewer.setLewisView(); break;
                case "it" : MapViewer.setITView(); break;
                case "nur" : MapViewer.setNursingView(); break;
                case "edu" : MapViewer.setEducationView(); break;
                case "bus" : MapViewer.setBusinessView(); break;
                case "adm" : MapViewer.setAdminView(); break;
            }

            System.out.println(description);
        }

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}